FROM python:3.9-alpine
ARG version=2022.8.1
MAINTAINER Eriks K
LABEL version=$version

WORKDIR /app
ENV PATH=$PATH:/app/.local/bin
COPY ./ebot /app/ebot
COPY setup.py requirements.txt /app/

RUN pip install -U --no-cache wheel pip setuptools \
 && python -OO /app/setup.py install \
 && mkdir player 
 
# && addgroup -S nocando  \
# && adduser -S nocando -G nocando \
# && chown -R nocando:nocando /app \
# && chmod -R 775 /app

#USER nocando
#COPY entrypoint.sh /entrypoint.sh
#ENTRYPOINT ["/entrypoint.sh"]
WORKDIR /app/player
CMD ["python", "-O", "-m", "ebot"]
