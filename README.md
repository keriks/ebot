# eBot

Script uses [eeriks/eRepublik][1] package to automate eRepublik gameplay

## How to run ebot
1. Install Docker/Docker Desktop
2. Create root folder where to place Your bots (eg `/home/user/ebot/`, `c:\\Users\user\ebot`)
3. Create a bot folder for each account inside root folder (eg `/home/user/ebot/player_a`, `c:\\Users\user\ebot\player_b`)
4. Copy `docker-compose.yaml` (text) file inside root folder (eg `/home/user/ebot/docker-compose.yaml`, `c:\\Users\user\ebot\docker-compose.yaml`) and change names accordingly:
```yaml
version: '3'

services:
    player_a:
        image: ebot
        volumes:
        -  ./player_a:/app/player
        restart: always
```
5. Build docker image: `docker build --build-arg version="development" --tag ebot .`
6. Create [config file for each account][2] and place them inside each of the bot's folder named `config.json` (eg `/home/user/ebot/player_a/config.json`, `c:\\Users\user\ebot\player_b\config.json`)
7. You're all done! To run the bots:
   - Linux/Unix/MacOS: execute from the ebot root folder `docker-compose up -d`
   - Windows: Somehow from the Desktop app run `docker-compose.yaml`. TBD

## How to run from source
### Setup on Linux/Unix/MacOS
1. Install latest Python (minimum required version is 3.8)
2. Clone repository
``` bash
git clone git@gitlab.com:keriks/ebot.git
cd ebot/
```
3. Create and activate virtual environment and install dependencies
``` bash
python3 -m venv venv
source venv/bin/activate
pip install -Ur requirements.txt
```
4. Configure default `config.json` file or delete it and run
``` bash
python -m ebot
```

### Setup on Windows
1. Install requirements:
   - [Python3][3] (minimum required version is 3.8)
   - [Git][4]

2. Clone repository (all future commands are meant to be run from `CMD` Start > Search cmd > Open
``` bash
git clone git@gitlab.com:keriks/ebot.git
cd erepublik-bot/
```
3. Create and activate virtual environment and install dependencies
``` bash
python3 -m venv venv
venv\Scripts\activate
pip install -Ur requirements.txt
```
4. Configure default `config.json` file or delete it and run
``` bash
python -m ebot
```

## Creating standalone executable
### Compilation on Linux/Unix/MacOS
``` bash
./upgrade.sh
./compile.sh
```

### Compilation on Windows
``` cmd
venv\Scripts\activate
compile.bat
```

## Running, updating standalone versions as services
### Running on Linux/Unix/MacOS

``` bash
#!/bin/bash
#Change variable 'bot' to desired value
bot="player_name"
_bot_exe="bot_${bot}"
#Create separate folder for (each) account (-p don't warn if folder exists)
mkdir -p $bot

# stop previous process to update executable
killall "$_bot_exe"

# Sleep 3 seconds to allow bot to exit gracefully
sleep 3

# find latest versions name
newver=$(ls -dtr1 dist/* | tail -1)

# Hard link latest version
cp -lfp "$newver" "${bot}/${_bot_exe}"
cd "${bot}"
./"${_bot_exe}" &
disown -h %1
```

### Running on Windows
1. Double click executable to run bot with open console
2. As background service using NSSM:
    1. [Download NSSM][5]
    2. open cmd prompt and enter `nssm install <servicename>` replace `<servicename>` with Your players name e.g., `nssm install bot_player_a`
    3. Follow instructions in [NSSM - usage][6]



[1]: https://github.com/eeriks/erepublik
[2]: https://eeriks.github.io/erepublik
[3]: https://www.python.org/downloads/windows/
[4]: https://git-scm.com/download/win
[5]: https://nssm.cc/download "NSSM - the Non-Sucking Service Manager"
[6]: https://nssm.cc/usage
