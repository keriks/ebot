import json
import os
import unittest

import requests
import responses

from ebot.aviator_support import AviatorCitizen, aviator_support


class AviatorCitizenTest(AviatorCitizen):
    def post(self, url: str, data: dict = None, json: dict = None, **kwargs):
        print("POST", url, data, json, kwargs)
        return super().post(url, data, json, **kwargs)

    def get(self, url: str, **kwargs):
        print("GET", url, kwargs)
        return super().get(url, **kwargs)


class TestAviatorSupport(unittest.TestCase):
    """Tests for `erepublik` eLatvian aviator support."""

    def setUp(self):
        os.environ["PYTHON_TESTS"] = "1"
        """Set up test fixtures, if any."""

    @responses.activate
    def test_simple(self):
        responses.add(
            responses.GET,
            "https://erep.lv/aviator/latest_article/",
            json=dict(status=True, free_food={"q1": 0}, article_id=2724438, week=166),
        )
        responses.add(responses.POST, "https://api.erep.lv/promos/add/", json=dict(status=True))
        responses.add(responses.POST, "https://api.erep.lv/bot/update", json=dict(status=True))
        responses.add(responses.POST, "https://api.erep.lv/bot/register", json=dict(status=True))
        responses.add(responses.POST, "https://erep.lv/aviator/check/1596281", json=dict(status=True))
        responses.add(responses.POST, "https://erep.lv/aviator/check/1607812", json=dict(status=True))
        responses.add(responses.POST, "https://erep.lv/aviator/check/4360535", json=dict(status=False))
        responses.add(responses.POST, "https://erep.lv/aviator/check/5365408", json=dict(status=True))

        # with open('fixtures/en.html') as f:
        with open("fixtures/en_login.html") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en", body=f.read())
            responses.add(
                responses.POST,
                url="https://www.erepublik.com/en/login",
                body=f.read(),
                status=302,
                headers={"Location": "https://www.erepublik.com/en"},
            )
            responses.add(responses.GET, url="https://www.erepublik.com/en/login", body=f.read())

        citizen = AviatorCitizenTest("email", "password")
        citizen.config.telegram = False

        with open("fixtures/en_login.html") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en", body=f.read())

        with open("fixtures/en_economy_inventory-items_.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/economy/inventory-items/", json=json.load(f))

        with open("fixtures/en_economy_mymarketoffers.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/economy/myMarketOffers/", json=json.load(f))

        with open("fixtures/en_main_articlecomments.json") as f:
            responses.add(responses.POST, "https://www.erepublik.com/en/main/articleComments", json=json.load(f))

        with open("fixtures/en_main_leaderboards-kills-aircraft-rankings_71_1_0_0.json") as f:
            responses.add(responses.GET, json=json.load(f), url="https://www.erepublik.com/en/main/leaderboards-kills-aircraft-rankings/71/1/0/0")

        with open("fixtures/citizen_profile_1596281.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/citizen-profile-json/1596281", json=json.load(f))

        with open("fixtures/citizen_profile_1607812.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/citizen-profile-json/1607812", json=json.load(f))

        with open("fixtures/citizen_profile_4360535.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/citizen-profile-json/4360535", json=json.load(f))

        with open("fixtures/citizen_profile_5365408.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/citizen-profile-json/5365408", json=json.load(f))

        with open("fixtures/city_699_residents.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/city-data/699/residents", json=json.load(f))

        with open("fixtures/city_706_residents.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/city-data/706/residents", json=json.load(f))

        with open("fixtures/city_709_residents.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/main/city-data/709/residents", json=json.load(f))

        with open("fixtures/military_unit_data.json") as f:
            responses.add(responses.GET, "https://www.erepublik.com/en/military/military-unit-data/", json=json.load(f))

        with open("fixtures/economy_marketplaceajax.json") as f:
            responses.add(responses.POST, "https://www.erepublik.com/en/economy/marketplaceAjax", json=json.load(f))

        with open("fixtures/economy_marketplaceactions.json") as f:
            responses.add(responses.POST, "https://www.erepublik.com/en/economy/marketplaceActions", json=json.load(f))

        with open("fixtures/economy_donate-items-action.html") as f:
            responses.add(responses.POST, "https://www.erepublik.com/en/economy/donate-items-action", body=f.read())

        aviator_support(citizen)
