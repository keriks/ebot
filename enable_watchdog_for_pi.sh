#!/bin/bash

# Description: Enabling Watchdog on Raspberry Pi
# Author: Eriks K
# Created: 2020-12-04
# Tutorial source: https://medium.com/@arslion/enabling-watchdog-on-raspberry-pi-b7e574dcba6b

# Stage 0: Check if root to ensure that commands can be executed
if [[ ! $UID -eq 0 ]]; then
    echo "You must be root to run this script! Please run with sudo: "
    echo "sudo ${0}"
    exit 1
fi

# single > writes to file, double >> appends to file

# Stage 1: Activating watchdog hardware in pi
cp /boot/config.txt /boot/config.txt.bak  # Backup

echo "# Enabling watchdog." >> /boot/config.txt
echo "dtparam=watchdog=on" >> /boot/config.txt

# Stage 2: Installing watchdog
apt-get install watchdog

# Stage 3: Configuring watchdog to respond to events
cp /etc/watchdog.conf /etc/watchdog.conf.bak  # Backup

echo 'max-load-1 = 24' >> /etc/watchdog.conf
echo 'min-memory = 1' >> /etc/watchdog.conf
echo 'watchdog-timeout=15' >> /etc/watchdog.conf
echo 'watchdog-device = /dev/watchdog' >> /etc/watchdog.conf

# Stage 4: Starting/Monitoring watchdog service:
systemctl start watchdog

# Stage 5: Add watchdog on boot.
cp /lib/systemd/system/watchdog.service /lib/systemd/system/watchdog.service.bak  # Backup
sed -i 's|^WantedBy=.*$|WantedBy=multi-user.target|' /lib/systemd/system/watchdog.service

systemctl enable watchdog

