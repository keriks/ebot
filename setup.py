#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import find_packages, setup

with open('requirements.txt') as f:
    requirements = f.read().split()

try:
    with open('requirements-dev.txt') as f:
        test_requirements = requirements + f.read().split()[2:]
except FileNotFoundError:
    test_requirements = requirements

setup(
    author="Eriks K",
    author_email="ebot@72.lv",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    description="Python package for automated eRepublik playing",
    entry_points={},
    install_requires=requirements,
    include_package_data=True,
    name="eRepublik_bot",
    packages=find_packages(include=["ebot"]),
    python_requires=">=3.8, <4",
    setup_requires=requirements,
    version="2022.8.1",
    zip_safe=False,
)
