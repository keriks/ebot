""" eBot
Copyright (C) 2022  Eriks K

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import logging
import os
import sys

from erepublik._logging import ErepublikFormatter, ErepublikFileHandler, ErepublikLogConsoleHandler
from erepublik.classes import ErepublikException
from erepublik.utils import json

from ebot.aviator_support import SupplierCitizen, LatvianSupply
from ebot.helpers import EbotErrorHttpHandler

logger = logging.getLogger("Aviator")

formatter = ErepublikFormatter()
file_handler = ErepublikFileHandler()
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.INFO)
console_handler = ErepublikLogConsoleHandler()
console_handler.setFormatter(formatter)
console_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)
error_handler = EbotErrorHttpHandler()
error_handler.setFormatter(formatter)
error_handler.setLevel(logging.ERROR)
logger.addHandler(error_handler)
logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("-e", "--email", type=str, default="", help="Email address")
        parser.add_argument("-p", "--password", type=str, default="", help="Password")
        parser.add_argument("-c", "--config", type=str, default="./config.json", help="Config file to use")
        parser.add_argument("-d", "--debug", default=False, action="store_true", help="Debug")
        parser.add_argument("-r", "--provisional", default=False, action="store_true", help="Provisional")
        parser.add_argument("-x", "--checkpoint", type=int, default=0, help="Checkpoint id")
        args = parser.parse_args()
    except Exception as e:
        logger.error(str(e), exc_info=True, stack_info=True)
        sys.exit(1)
    player = SupplierCitizen("", "")
    try:
        player = SupplierCitizen.load_from_dump("SupplierCitizen__dump.json")
    except Exception as e:
        player.report_error(str(e))

    if not player.logged_in:
        if not args.email or not args.password:
            with open(args.config, "r") as f:
                configs = json.load(f)
            e = configs["email"]
            p = configs["password"]
            del f, configs
        else:
            e = args.email
            p = args.password

        player = SupplierCitizen(e, p)
        del e, p

    player.set_debug(bool(args.debug or args.provisional))
    try:
        player.update()
        player.dump_instance()
        supply = LatvianSupply(player, args.debug, args.provisional)
        supply(checkpoint_id=args.checkpoint)
    except ErepublikException as e:
        print(e)
        player.report_error(str(e))
    except Exception as e:  # noqa
        player.report_error(str(e))
